function get_msg_tag_buttons(msgid,cnt,post)
{
	var tags = '<span class="tagbuttons '+msgid+'">' +
		'<input tabindex="-1" title="Ctrl+B" name="b" class="btn btn_mini btnbold" type="button" value="Bold" onclick="txtTag(\'b\',\''+msgid+'\')"/>' +
		'<input tabindex="-1" title="Ctrl+I" name="i" class="btn btn_mini btnitalic" type="button" value="Italic" onclick="txtTag(\'i\',\''+msgid+'\')"/>';
	if(post==1)
		tags += '<input tabindex="-1" title="Ctrl+S" name="spoiler" class="btn btn_mini" type="button" value="Spoiler" onclick="txtTag(\'spoiler\',\''+msgid+'\')"/>' +
		'<input tabindex="-1" title="Ctrl+E" name="cite" class="btn btn_mini btncite" type="button" value="Cite" onclick="txtTag(\'cite\',\''+msgid+'\')"/>' +
		'<input tabindex="-1" title="Ctrl+Q" name="quote" class="btn btn_mini" type="button" value="Quote" onclick="txtTag(\'quote\',\''+msgid+'\')"/>' +
		'<input tabindex="-1" title="Ctrl+D" name="code" class="btn btn_mini btncode" type="button" value="Code" onclick="txtTag(\'code\',\''+msgid+'\')"/>';
	tags += '</span>';
	if(!post)
		tags += '<br/>';
	var ctrlBtn = false;
	var ctrlHeld = false;
	var msgArea = $('textarea[name="'+msgid+'"]');

	$(msgArea).before(tags);

	$(msgArea).blur(function(){ctrlBtn=false;ctrlHeld=false;});

	$(msgArea).keyup(function(e){
		if(e.which==17){ctrlBtn=false;ctrlHeld=false;}
		if(e.which==18 && ctrlHeld) ctrlBtn=true;
		if(post==1)
			msg_cc($(msgArea),cnt,'post');
	});

	$(msgArea).keydown(function(e){
		if(e.which==17){ctrlBtn=true;ctrlHeld=true;}
		if(e.which==18) ctrlBtn=false;
		if(e.which==66 && ctrlBtn){e.stopPropagation();e.preventDefault();txtTag('b',msgid);}
		else if(e.which==68 && ctrlBtn && post==1){e.stopPropagation();e.preventDefault();txtTag('code',msgid);}
		else if(e.which==69 && ctrlBtn && post==1){e.stopPropagation();e.preventDefault();txtTag('cite',msgid);}
		else if(e.which==73 && ctrlBtn){e.stopPropagation();e.preventDefault();txtTag('i',msgid);}
		else if(e.which==81 && ctrlBtn && post==1){e.stopPropagation();e.preventDefault();txtTag('quote',msgid);}
		else if(e.which==83 && ctrlBtn && post==1){e.stopPropagation();e.preventDefault();txtTag('spoiler',msgid);}
		if(post==1)
			msg_cc($(msgArea),cnt,'post');
	});

	if(post==1)
		msg_cc($(msgArea),cnt,'post');
	return;
}

function msg_cc(field,cnt,type)
{
	var msg = $(field);
	var cntdisp = (cnt*0.25);
	if($(field).val().length < (cnt * 0.75))
	{
		$('.charcount.'+$(field).attr('name')).empty();
		return;
	}
	if(type=='post')
		cnt -= (($(field).val().split(/\n|\r|"/).length - 1) * 5) + (($(field).val().split(/&/).length - 1) * 4) + (($(field).val().split(/>|</).length - 1) * 3);
	else if(type=='pm')
		cnt -= (($(field).val().split(/\n|\r/).length) - 1);
	cnt -= ($(msg).val().length);
	var cc = $('.charcount.'+$(field).attr('name'));
	if(cnt<=cntdisp)
		$(cc).html(cnt+' characters remaining');
	else
		$(cc).empty();
	$(cc).css(cnt<0 ? {color:'#f00'} : {color:''});
}

function sub_cc(field,type)
{
	var sub = $(field);
	if(type=='topictitle')
	{
		if($(sub).val().match(/[\u0100-\u2017\u201E-\uffff]/))
		{
			$('.charcount.'+$(field).attr('name')).html('Warning: Unicode characters are removed from topic titles');
			return;
		}
		var cnt = 80;
		var cntdisp = 20;
		cnt -= $(sub).val().length + (($(field).val().split(/>|</).length - 1) * 3) + (($(field).val().split(/&/).length - 1) * 4) + (($(field).val().split(/"/).length - 1) * 5);
	}
	else if(type=='subject')
	{
		if($(sub).val().match(/[\u0100-\u2017\u201E-\uffff]/))
		{
			$('.charcount.'+$(field).attr('name')).html('Warning: Unicode characters are removed from PM subjects');
			return;
		}
		var cnt = 100;
		var cntdisp = 25;
		cnt -= $(sub).val().length;
	}
	else if(type=='poll_text')
	{
		if($(sub).val().match(/[\u0100-\u2017\u201E-\uffff]/))
		{
			$('.charcount.'+$(field).attr('name')).html('Warning: Unicode characters are removed from poll titles');
			return;
		}
		var cnt = 200;
		var cntdisp = 50;
		cnt -= $(sub).val().length + (($(field).val().split(/>|</).length - 1) * 3) + (($(field).val().split(/&/).length - 1) * 4) + (($(field).val().split(/"/).length - 1) * 5);
	}
	var cc = $('.charcount.'+$(field).attr('name'));
	if(type=='topictitle' && cnt<=cntdisp)
		$(cc).html(cnt+' characters remaining (5-80 characters allowed)');
	else if(type=='subject' && cnt<=cntdisp)
		$(cc).html(cnt+' characters remaining (2-100 characters allowed)');
	else if(type=='poll_text' && cnt<=cntdisp)
		$(cc).html(cnt+' characters remaining (5-200 characters allowed)');
	else
		$(cc).empty();
	$(cc).css(cnt<0 ? {color:'#f00'} : {color:''});
}

function txtTag(tag,name)
{
	var msgArea = $('textarea[name="'+name+'"]');
	var currTag = $('.tagbuttons.'+name+' input[name="'+tag+'"]');
	var tagStart = "<"+tag+">";
	var tagEnd = "</"+tag+">";
	var c = $(msgArea)[0].selectionStart;
	var selPre = $(msgArea).val().substr(0,c);
	var selPost = $(msgArea).val().substr($(msgArea)[0].selectionEnd);
	var selTxt;

	if(c!=undefined)
	{
		selTxt = $(msgArea).val().substr(c,$(msgArea)[0].selectionEnd-c);
	}
	if(selTxt.length<1)
	{
		if($(currTag).hasClass('active'))
		{
			$(msgArea).val([$(msgArea).val().slice(0,c),tagEnd,$(msgArea).val().slice(c)].join(''));
			$(currTag).removeClass('active').css('color','#000');
			var p = c+tagEnd.length;
			setPos($(msgArea),p);
		}
		else
		{
			$(msgArea).val([$(msgArea).val().slice(0,c),tagStart,$(msgArea).val().slice(c)].join(''));
			var p = c+tagStart.length;
			$(currTag).addClass('active').css('color','#6564ff');
			setPos($(msgArea),p);
		}
	}
	else
	{
		$(msgArea).val(selPre+tagStart+selTxt+tagEnd+selPost);
		var p = c+tagStart.length+selTxt.length+tagEnd.length;
		setPos($(msgArea),p);
	}
}

function setPos(m,p)
{
	if($(m)[0].setSelectionRange)
	{
		$(m)[0].focus();
		$(m)[0].setSelectionRange(p,p);
	}
	else if($(m)[0].createTextRange)
	{
		var r = $(m)[0].createTextRange();
		r.collapse(true);
		r.moveEnd('character',p);
		r.moveStart('character',p);
		r.select();
	}
}

function quick_quote(board_id,topic_id,message_id,xsrf_key,wysiwyg)
{
	if(window.getSelection().toString().length>0)
	{
		var mb = window.getSelection().getRangeAt(0).startContainer;
		var msghi = mb.parentNode.getAttribute('name');
		if(msghi==null)
		{
			while(mb.className!='msg_body' && mb.className!='msg_body newbeta')
				mb = mb.parentNode;
			msghi = mb.getAttribute('name');
		}
		var msghitxt = window.getSelection().toString();
		if(msghi==message_id)
		{
			$.ajax({
				type: 'POST',
				url: '/ajax/forum_quote_message',
				data: {bid: board_id, tid: topic_id, mid: message_id, hi: 1, key: xsrf_key},
				success: function(response)
				{
					var d = response.quote;
					if(response.error)
					{
						alert(clean_alert(response.error));
					}
					else if(wysiwyg)
					{
						d += "<br /><blockquote>"+msghitxt.replace(/\r\n|\r|\n/gi, "<br />")+"</blockquote>";
						if($('#quill_message_new .ql-editor').text())
							quill_new.clipboard.dangerouslyPasteHTML($('#quill_message_new .ql-editor').html() + d);
						else
							quill_new.clipboard.dangerouslyPasteHTML(d);
						quill_new.insertText(quill_new.getLength(), '\n');
						quill_new.setSelection(quill_new.getLength());
						quill_new.focus();

					}
					else
					{
						var msg = $('textarea[name="messagetext"]');
						d += "&lt;quote&gt;"+msghitxt+"&lt;/quote&gt;";
						var s = d.replace(/\&lt;/gi,"<").replace(/\&gt;/gi,">").replace(/\&amp;/gi,"&").replace(/\&quot;/gi,'"');
						$(msg).val($(msg).val()+s+"\r");
						var val = $(msg).val();
						setPos(document.getElementsByName('messagetext')[0],val.length);
					}
				}
			});
		}
	}
	else
	{
		$.ajax({
			type: 'POST',
			url: '/ajax/forum_quote_message',
			data: {bid: board_id, tid: topic_id, mid: message_id, wysi: wysiwyg, key: xsrf_key},
			success: function(response)
			{
				var d = response.quote;
				if(response.error)
				{
					alert(clean_alert(response.error));
				}
				else if(wysiwyg)
				{
					if($('#quill_message_new .ql-editor').text())
						quill_new.clipboard.dangerouslyPasteHTML($('#quill_message_new .ql-editor').html() + d);
					else
						quill_new.clipboard.dangerouslyPasteHTML(d);
					quill_new.insertText(quill_new.getLength(), '\n');
					quill_new.setSelection(quill_new.getLength());
					quill_new.focus();

				}
				else
				{
					var msg = $('textarea[name="messagetext"]');
					var s = d.replace(/\&lt;/gi,"<").replace(/\&gt;/gi,">").replace(/\&amp;/gi,"&").replace(/\&quot;/gi,'"');
					$(msg).val($(msg).val() + s);
					var val = $(msg).val();
					setPos(document.getElementsByName('messagetext')[0],val.length);
				}
			}
		});
	}
}

function post_new_message(board_id, topic_id, xsrf_key)
{
	if($("#quill_message_new .ql-editor").html().length > 1024 * 1024 * 4)
	{
		alert("The maximum size of a post with images is 4MB. You need to remove some images or include smaller versions of them.");
		return;
	}

	$("#post_new_message").prop('disabled', true).text('Processing...');

	$.ajax({
		type: 'POST',
		url: '/ajax/forum_post_message',
		data: {board: board_id, topic: topic_id, message: $("#quill_message_new .ql-editor").html(), key: xsrf_key, override: override},
		success: function(d)
		{
			if(d.status=='error')
			{
				alert(clean_alert(d.status_text));
				$("#post_new_message").prop('disabled', false).text('Post New Message');
			}
			else if(d.status=='warning')
			{
				if(warning_prompt(clean_alert(d.warnings)))
				{
					override = 1;
					post_new_message(board_id, topic_id, xsrf_key);
				}
				else
					$("#post_new_message").prop('disabled', false).text('Post New Message');
			}
			else
			{
				var new_location = d.message_url.substr(0, d.message_url.indexOf('#'));
				var current_location = window.location.pathname + window.location.search;
				window.location.href = d.message_url;
				if(new_location == current_location)
					location.reload(true);
			}


		}
	});
}

function edit_message(board_id, topic_id, message_id, div_id, xsrf_key)
{
	$.ajax({
		type: 'POST',
		url: '/ajax/forum_get_edit_message',
		data: {board: board_id, topic: topic_id, message: message_id, div_id: div_id, key: xsrf_key},
		success: function(response)
		{
			if(response)
			{
				if($("#msg_edit").attr('data-id'))
				{
					$("#msg_" + $("#msg_edit").attr('data-id') + " .msg_body").show();
					$("#msg_" + $("#msg_edit").attr('data-id') + " .msg_below").show();
				}

				$("#msg_edit").remove();
				$("#msg_" + div_id + " .msg_body").hide();
				$("#msg_" + div_id + " .msg_below").hide();
				$("#msg_" + div_id).prepend('<div id="msg_edit" data-id="' + div_id + '"></div>');
				$("#msg_edit").html(response);
			}
			else
			{
				$("#msg_" + div_id + " .action_after").text("This message cannot be edited at this time");
			}
		}
	});

}

function cancel_edit(div_id)
{
	$("#msg_" + div_id + " .msg_body").show();
	$("#msg_" + div_id + " .msg_below").show();
	$("#msg_edit").remove();
}

function save_edit(board_id, topic_id, message_id, xsrf_key)
{
	if($("#quill_message_edit .ql-editor").html().length > 1024 * 1024 * 4)
	{
		alert("The maximum size of a post with images is 4MB. You need to remove some images or include smaller versions of them.");
		return;
	}

	$("#save_edit").prop('disabled', true).text('Processing...');

	$.ajax({
		type: 'POST',
		url: '/ajax/forum_save_edit_message',
		data: {board: board_id, topic: topic_id, message: message_id, message_text: $("#quill_message_edit .ql-editor").html(), key: xsrf_key, override: override},
		success: function(d)
		{
			if(d.status=='error')
			{
				alert(clean_alert(d.status_text));
				$("#save_edit").prop('disabled', false).text('Save Changes');

			}
			else if(d.status=='warning')
			{
				if(warning_prompt(clean_alert(d.warnings)))
				{
					override = 1;
					save_edit(board_id, topic_id, message_id, xsrf_key);
				}
				else
					$("#save_edit").prop('disabled', false).text('Save Changes');
			}
			else
			{
				window.location.href = d.message_url;
				location.reload(true);
			}
		}
	});
}

function post_new_topic(board_id, xsrf_key)
{
	if($("#quill_message_topic .ql-editor").html().length > 1024 * 1024 * 4)
	{
		alert("The maximum size of a post with images is 4MB. You need to remove some images or include smaller versions of them.");
		return;
	}

	$("#post_new_topic").prop('disabled', true).text('Processing...');

	var poll_data = {};
	if($("#create_poll").val())
	{
		poll_data['poll_title'] = $("#poll_title").val();
		poll_data['min_level'] = $("#min_level").val();
		var poll_option = {};
		for(var i=1;i<=10;i++)
		{
			if($("#poll_option_" + i).val())
				poll_option[i] = $("#poll_option_" + i).val();
		}
		poll_data['poll_option'] = poll_option;
	}

	$.ajax({
		type: 'POST',
		url: '/ajax/forum_post_topic',
		data: {board: board_id, topic: $("#topic_title").val(), flair: $("#flair").val(), poll: JSON.stringify(poll_data), message: $("#quill_message_topic .ql-editor").html(), key: xsrf_key},
		success: function(d)
		{
			if(d.status=='error')
			{
				alert(clean_alert(d.status_text));
				$("#post_new_topic").prop('disabled', false).text('Create New Topic');
			}
			else if(d.status=='warning')
			{
				if(warning_prompt(clean_alert(d.warnings)))
				{
					override = 1;
					post_new_topic(board_id, xsrf_key);
				}
				else
					$("#post_new_topic").prop('disabled', false).text('Create New Topic');
			}
			else
			{
				window.location.href = d.topic_url;
			}

		}
	});
}

function preview_new_message(board_id, topic_id, xsrf_key)
{
	if($("#quill_message_new .ql-editor").html().length > 1024 * 1024 * 4)
	{
		alert("The maximum size of a post with images is 4MB. You need to remove some images or include smaller versions of them.");
		return;
	}

	$.ajax({
		type: 'POST',
		url: '/ajax/forum_preview_message',
		data: {board: board_id, topic: topic_id, message: $("#quill_message_new .ql-editor").html(), key: xsrf_key, override: override},
		success: function(d)
		{
			if(d.status=='error')
			{
				alert(clean_alert(d.status_text));
			}
			else
			{
				$("#quoted_message").html(d.formatted_message_text);
				$("#quoted_message").dialog({resizable: true, dialogClass: "reg_dialog", closeText: "X", height: "auto", maxHeight: $(window).height(), width: "80%", modal: true, open: function(){$('.ui-widget-overlay').bind('click',function(){$('#quoted_message').dialog('close');});} });
			}


		}
	});
}
